# cs5-strata-v2

Strata Issue Tracker Web App, Version 2. Flask, Python 3.7. <br> 
Constructed as a requirement of the Code.Sydney Rapid Flask Development Course. <br>
<a href='https://docs.google.com/document/d/1HJaV0AhNh5yMsEUWjZ6wJIKZ1U4R813kEGqmC_GnXK8/edit'>You can find the requirements for Phase 2 here.</a>