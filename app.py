"""
Strata Flask app V2 - 20190404 - Code.Sydney | Geoff Pidcock

#ToDo
- Implement duplicate user screening
"""

from flask import Flask, render_template, g, request, session, redirect, url_for
from werkzeug.security import generate_password_hash, check_password_hash
from flaskext.mysql import MySQL
import os

# instantiate app
app = Flask(__name__)

# Config app to connect to MySQL DB
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'admin'
app.config['MYSQL_DATABASE_DB'] = 'strata'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'

# Config app to lock sessions using random key
app.config['SECRET_KEY'] = os.urandom(24)

# instantiate db
flaskmysql = MySQL()
flaskmysql.init_app(app)

# Define helper functions
def connect_db(flaskmysql):
    con = flaskmysql.connect()
    return con

def get_db(flaskmysql):
    if not hasattr(g, 'mysql_db'):
        g.mysql_db = connect_db(flaskmysql)
    return g.mysql_db

def Convert(tup, di):
    # Takes a tuple and a dict, and populates the dict with values from the tuple.
    di = dict(tup)
    return di

def get_current_user():
    user_result = None

    if 'user' in session:
        user = session['user']
        db = get_db(flaskmysql)
        cur = db.cursor()
        cur.execute('select id, email, password_hash, admin_role from users where email = (%s)',(user))
        user_tuple = cur.fetchone()
        user_dict = dict()
        user_dict['id'] = user_tuple[0]
        user_dict['email'] = user_tuple[1]
        user_dict['password_hash'] = user_tuple[2]
        user_dict['admin_role'] = user_tuple[3]
        user_result = user_dict

    return user_result



# Close DB connection after a transaction
@app.teardown_appcontext
def close_db(error):
    if hasattr(g, 'mysql_db'):
        g.mysql_db.close()

@app.route('/')
def index():
    user = get_current_user()
    con = get_db(flaskmysql)
    cur = con.cursor()
    cur.execute('select issue_number, issue_description from issues')
    results = cur.fetchall()
    dictionary = dict()
    results_dict = Convert(results,dictionary)
    return render_template('index.html',user=user,results_dict=results_dict)

@app.route('/create', methods=['GET', 'POST'])
def create():
    if request.method == 'GET':
        return render_template('create.html')
    else:
        description = request.form['Description']
        con = get_db(flaskmysql)
        cur = con.cursor()
        cur.execute('insert into issues (issue_description) values (%s)', (description))
        con.commit()
        return redirect(url_for('index'))

@app.route('/edit', methods=['POST', 'GET'], defaults={'key' : '1'})
@app.route('/edit/<string:key>', methods=['GET', 'POST'])
def edit(key):
    if request.method == 'GET':
        user = get_current_user()
        con = get_db(flaskmysql)
        cur = con.cursor()
        cur.execute('select issue_number, issue_description from issues where issue_number = (%s)', (key))
        results = cur.fetchall()
        return render_template('edit.html',results=results, user = user)
    else:
        if request.form['Type'] == 'Update':
            description = request.form['Description']
            con = get_db(flaskmysql)
            cur = con.cursor()
            cur.execute('update issues \
                        set issue_description = (%s) \
                        where issue_number = (%s);',
                        (description, key))
            con.commit()
        elif request.form['Type'] == 'Delete':
            con = get_db(flaskmysql)
            cur = con.cursor()
            cur.execute('delete from issues where issue_number = (%s);',(key))
            con.commit()
        return redirect(url_for('index'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    user = get_current_user()

    if request.method == 'POST':

        con = get_db(flaskmysql)
        cur = con.cursor()
        hashed_password = generate_password_hash(request.form['password'], method='sha256')
        cur.execute('insert into users (email, password_hash, admin_role) values (%s, %s, %s)',(request.form['email'], hashed_password, '0'))
        con.commit()

        session['user'] = request.form['email']

        return redirect(url_for('index'))
    else:
        return render_template('register.html', user=user)

@app.route('/login', methods=['GET', 'POST'])
def login():
    user = get_current_user()

    if request.method == 'POST':
        con = get_db(flaskmysql)
        cur = con.cursor()

        email = request.form['email']
        password = request.form['password']

        cur.execute('select id, email, password_hash from users where email = (%s)', (email))
        user_result = cur.fetchone()

        if check_password_hash(user_result[2], password):
            session['user'] = user_result[1]
            return redirect(url_for('index'))
        else:
            return '<h1>The password is incorrect!</h1>'

    else:
        return render_template('login.html', user=user)

@app.route('/logout')
def logout():
    session.pop('user', None)
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(debug=True)